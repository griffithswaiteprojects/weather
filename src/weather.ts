


import {readDataFile, writeDataFile} from "./file-util";

export const calculate = (inputFileName : string): string =>{
    // Utility used to deal with file processing
    const lines = readDataFile('data/'+inputFileName+".csv");

    // TODO : need to add logic to parse the lines to produce the aggregate information

    const resultFileName = 'data/'+inputFileName+'-result.csv'
    writeDataFile(resultFileName, ['City,Min,Max,Average', 'City1,Min1,Max1,Average1', 'City2,Min2,Max2,Average2']);
    return resultFileName;
}