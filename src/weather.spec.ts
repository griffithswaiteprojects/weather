import {calculate} from "./weather";
import {readDataFile} from "./file-util";

// Given a raw data file containing temperatures from around the would,
// produce a file containing the min,max and average temputures per city

describe('Example File', ():void =>{
    test('Using example1 - Birmingham and Brisbane', (): void => {
        const resultFile = calculate('example1')
        
        const expectedResults = [
            'City,Min,Max,Average',
            'Birmingham,-5,12,3.6',
            'Brisbane,19,32,27.6']
        expect(readDataFile(resultFile)).toBe(expectedResults);
    });
})